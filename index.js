#!/usr/bin/env node

const fs = require('fs')
const Kafka = require('node-rdkafka')

// kafka-offset-reset --brokers localhost:9093 --topic test --group test --partition 0 --offset 200000 --dry-run -- ssl.key.location=uat.key ssl.certificate.location=uat.key ssl.ca.location=ca.crt security.protocol=ssl
const argv = require('minimist')(process.argv.slice(2), {
  string: ['topic', 'brokers', 'group'],
  '--': true,
  boolean: ['dry-run', 'help']
})

function help() {
  console.error('kafka-offset-reset --brokers <brokerstring> --topic <topic> --group <group> --partition <partition> --offset <new-offset> [--dry-run] -- [librdkafka args]')
  console.error('Example:\n\tkafka-offset-reset --brokers localhost:9093 --topic test --group test --partition 0 --offset 200000 --dry-run -- ssl.key.location=uat.key ssl.certificate.location=uat.key ssl.ca.location=ca.crt security.protocol=ssl')
}

if (argv.help) {
  help()
  process.exit(0)
}

if (!(argv.topic && argv.brokers && argv.group && (argv.partition === 0 || argv.partition) && argv.offset)) {
  console.error('Missing required arguments\n')
  help()
  process.exit(1)
}

const librdkafkaParams = argv['--'].reduce((acc, curr) => {
  const kv = curr.split('=', 2)
  return {
    ...acc,
    [kv[0]]: kv[1]
  }
}, {})

const consumer = new Kafka.KafkaConsumer({
  'group.id': argv.group,
  'metadata.broker.list': argv.brokers,
  ...librdkafkaParams,
  'offset_commit_cb': function(err, topicPartitions) {

    if (err) {
      // There was an error committing
      console.error('Error committing');
      console.error(err);
    } else {
      // Commit went through. Let's log the topic partitions
      console.log('Committed: %o', topicPartitions)
    }
  }
})

consumer.on('event.log', function(err, data) {
  console.error(JSON.stringify(err))
})

consumer.on('ready', function(i, metadata) {
  if (argv['dry-run']) {
    console.error('--dry-run: commit %o', { topic: argv.topic, partition: argv.partition, offset: argv.offset })
  } else {
    consumer.commit({ topic: argv.topic, partition: argv.partition, offset: argv.offset })
  }

  consumer.disconnect()
})

consumer.connect()
