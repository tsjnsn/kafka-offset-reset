# Overview

Simple, straightforward tool to reset kafka offsets with librdkafka. Use at your own risk.

# Installing

npm install -g kafka-offset-reset

# CLI options
```bash
kafka-offset-reset --brokers <brokerstring> --topic <topic> --group <group> --partition <partition> --offset <new-offset> [--dry-run] -- [librdkafka args]
```

# Example

```bash
kafka-offset-reset --brokers localhost:9093 --topic test --group test --partition 0 --offset 200000 --dry-run -- ssl.key.location=uat.key ssl.certificate.location=uat.key ssl.ca.location=ca.crt security.protocol=ssl
```
