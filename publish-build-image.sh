#!/usr/bin/env bash

docker build -t registry.gitlab.com/tsjnsn/kafka-offset-reset:build --file build.Dockerfile .
docker push registry.gitlab.com/tsjnsn/kafka-offset-reset:build
